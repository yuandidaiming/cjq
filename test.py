import allure
import pytest


def add(a, b):
    return a + b


@allure.feature("计算机器加法测试")
class TestAdd:

    @allure.severity(allure.severity_level.NORMAL)
    @allure.story("加法运算")
    @allure.tag('hebeu')
    def test_add_positive(self):
        allure.dynamic.title("测试正数相加")
        allure.dynamic.description("测试两个正数相加的结果")
        print("开始计算")
        result = add(1, 2)
        assert result == 3
        print("结束计算")


if __name__ == '__main__':
    pytest.main(['-s', '-v', '--alluredir=some/allure/dir'])
    print("结束测试")
